package mpp.kpj;

import mpp.kpj.time.PrintCurrentTime;
import mpp.kpj.time.PrintCurrentTimeImplementationPicker;

public class Main {

    public static void main(String[] args) {
        int choiceNumber = Integer.parseInt(args[0]);
        PrintCurrentTime printCurrentTime = PrintCurrentTimeImplementationPicker.getImplementation(choiceNumber);

        System.out.println(printCurrentTime.print());
    }
}
