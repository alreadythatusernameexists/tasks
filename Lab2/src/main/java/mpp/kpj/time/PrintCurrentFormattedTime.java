package mpp.kpj.time;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PrintCurrentFormattedTime implements PrintCurrentTime {

    public String print() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        return dateTimeFormatter.format(LocalDateTime.now());
    }
}
