package mpp.kpj.time;

public class PrintCurrentTimeImplementationPicker {

    public static PrintCurrentTime getImplementation(int choiceNumber) {
        switch (choiceNumber) {
        case 1:
            return new PrintCalendarCurrentTime();
        case 2:
            return new PrintCurrentFormattedTime();
        default:
            return new PrintCurrentLocalTime();
        }
    }
}
