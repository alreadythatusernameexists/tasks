package mpp.kpj.time;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PrintCalendarCurrentTime implements PrintCurrentTime {

    public String print() {
        return new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
    }
}
