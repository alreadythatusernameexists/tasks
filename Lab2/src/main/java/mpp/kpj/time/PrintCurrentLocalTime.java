package mpp.kpj.time;

import java.time.LocalTime;

public class PrintCurrentLocalTime implements PrintCurrentTime {

    public String print() {
        return LocalTime.now().toString();
    }
}
