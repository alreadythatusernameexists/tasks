import ShadeMaker from "./ShadeMaker.js";
import BufferInitializer from "./BufferInitializer.js";
import TextureLoader from "./TextureLoader.js";
import SceneMaker from "./SceneMaker.js";

export default class Main {
  constructor() {
    this.shader = new ShadeMaker();
    this.bufferInitializer = new BufferInitializer();
    this.textureLoader = new TextureLoader();
  }

  checkGlContext(glContext) {
    if (!glContext) {
      alert('Unable to initialize WebGL. Your browser or machine may not support it.');
      return;
    }
  }
  
  start() {
    const canvas = document.querySelector('#glcanvas');
    const glContext = canvas.getContext('webgl');

    this.checkGlContext(glContext);

    const programInfo = this.shader.getShaderProgramInfo(glContext)

    const buffers = this.bufferInitializer.initBuffers(glContext);

    const texture = this.textureLoader.loadTexture(glContext, 'cubetexture.png');

    var sceneMaker = new SceneMaker();
    var then = 0;

    function render(now) {
      now *= 0.001;  // convert to seconds
      const deltaTime = now - then;
      then = now;

      sceneMaker.drawScene(glContext, programInfo, buffers, texture, deltaTime);

      requestAnimationFrame(render);
    }
    requestAnimationFrame(render);
  }
}

let main = new Main();
main.start();
