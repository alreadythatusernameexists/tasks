import Scene from "./webgl/scene.js";
import Shader from "./webgl/shader.js";
import Buffer from "./webgl/buffer.js";
import Texture from "./webgl/texture.js";

export default class Main {

    constructor() {
        let canvas = document.querySelector('#glcanvas');
        let gl = canvas.getContext('webgl');
        let scene = new Scene();
        let shader = new Shader();
        let buffer = new Buffer();
        let texture = new Texture();

        if (!gl) {
            alert('Unable to initialize WebGL. Your browser or machine may not support it.');
            return;
        }

        let buffers = buffer.initBuffers(gl);
        let loadedTexture = texture.loadTexture(gl, 'cubetexture.png');
        let time = 0;

        function render(now) {
            now *= 0.001;  // convert to seconds
            const deltaTime = now - time;
            time = now;
            scene.drawScene(gl, shader.getProgramInfo(gl), buffers, loadedTexture, deltaTime);
            requestAnimationFrame(render);
        }

        requestAnimationFrame(render);
    }

}

let main = new Main();
